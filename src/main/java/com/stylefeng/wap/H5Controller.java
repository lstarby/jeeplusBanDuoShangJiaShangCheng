package com.stylefeng.wap;

import com.alibaba.fastjson.JSONObject;

import com.stylefeng.guns.modular.shop.service.ITMemberService;
import com.stylefeng.guns.persistence.shop.model.TMember;
import com.stylefeng.web.utils.MemberUtils;
import com.stylefeng.web.utils.SessionUtil;
import com.stylefeng.web.wx.Constant;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;



/**
 * 推广员用户相关
 *
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2017/6/27  下午5:46
 */
@Controller
@RequestMapping(Constant.WX_H5_URI)
public class H5Controller extends com.stylefeng.web.controller.BaseController {
    private static final Logger logger = LoggerFactory.getLogger(H5Controller.class);
    SessionUtil sessionUtil = new SessionUtil();

    @Resource
    ITMemberService itMemberService;

    /**
     * 注册
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/login", method = {RequestMethod.POST, RequestMethod.GET})
    public String signup(HttpServletResponse response,
                         HttpServletRequest request,
                         @ModelAttribute TMember user,
                         @RequestParam(required = false) String token,
                         Model model) {
        logger.info("signup page start----");

        //get请求是，set
        if (request.getMethod().equals(RequestMethod.GET.name()) && StringUtils.isNoneBlank(token)){
            user = sessionUtil.getCurrentUser(token);
            model.addAttribute("user", user);
            model.addAttribute("city", user.getWxnickname());
        }else{
            model.addAttribute("city", "未登录");
        }


        return "/wap/index.html";
    }

}
