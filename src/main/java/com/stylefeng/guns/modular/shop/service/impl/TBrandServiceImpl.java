package com.stylefeng.guns.modular.shop.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.shop.service.ITBrandService;
import com.stylefeng.guns.modular.shop.service.ITFloorService;
import com.stylefeng.guns.persistence.shop.dao.TBrandMapper;
import com.stylefeng.guns.persistence.shop.dao.TFloorMapper;
import com.stylefeng.guns.persistence.shop.model.TBrand;
import com.stylefeng.guns.persistence.shop.model.TFloor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品类型表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TBrandServiceImpl extends ServiceImpl<TBrandMapper, TBrand> implements ITBrandService {
	
}
