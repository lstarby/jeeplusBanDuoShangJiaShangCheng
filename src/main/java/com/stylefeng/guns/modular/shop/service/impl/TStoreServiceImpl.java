package com.stylefeng.guns.modular.shop.service.impl;

import com.stylefeng.guns.persistence.shop.model.TStore;
import com.stylefeng.guns.persistence.shop.dao.TStoreMapper;
import com.stylefeng.guns.modular.shop.service.ITStoreService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Service
public class TStoreServiceImpl extends ServiceImpl<TStoreMapper, TStore> implements ITStoreService {
	
}
