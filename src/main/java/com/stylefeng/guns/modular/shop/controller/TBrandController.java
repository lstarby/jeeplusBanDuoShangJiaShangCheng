package com.stylefeng.guns.modular.shop.controller;

import com.stylefeng.guns.common.annotion.Permission;
import com.stylefeng.guns.common.annotion.log.BussinessLog;
import com.stylefeng.guns.common.constant.Const;
import com.stylefeng.guns.common.constant.Dict;
import com.stylefeng.guns.common.constant.factory.ConstantFactory;
import com.stylefeng.guns.common.controller.BaseController;
import com.stylefeng.guns.common.exception.BizExceptionEnum;
import com.stylefeng.guns.common.exception.BussinessException;
import com.stylefeng.guns.core.log.LogObjectHolder;
import com.stylefeng.guns.core.util.ToolUtil;
import com.stylefeng.guns.modular.shop.service.ITFloorService;
import com.stylefeng.guns.modular.system.warpper.DeptWarpper;
import com.stylefeng.guns.persistence.shop.model.TFloor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 商品类型表 前端控制器
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
@Controller
@RequestMapping("/tBrand")
public class TBrandController extends BaseController {
    private String PREFIX = "/shop/tBrand/";

    @Resource
    ITFloorService itBrandService;


    /**
     * 跳转到部门管理首页
     */
    @RequestMapping("")
    public String index() {
        return PREFIX + "tBrand.html";
    }

    /**
     * 跳转到添加部门
     */
    @RequestMapping("/tBrand_add")
    public String tBrandAdd() {
        return PREFIX + "tBrand_add.html";
    }

    /**
     * 跳转到修改部门
     */
    @RequestMapping("/tBrand_update/{tBrandId}")
    public String tBrandUpdate(@PathVariable Integer tBrandId, Model model) {
        TFloor tBrand = itBrandService.selectById(tBrandId);
        model.addAttribute("tBrand",tBrand);
        LogObjectHolder.me().set(tBrand);
        return PREFIX + "tBrand_edit.html";
    }


    /**
     * 新增部门
     */
    @BussinessLog(value = "添加部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/add")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object add(TFloor tBrand) {
        return this.itBrandService.insert(tBrand);
    }

    /**
     * 获取所有部门列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Object list(String condition) {
        List<Map<String, Object>> list = this.itBrandService.selectMaps(null);
        return super.warpObject(new DeptWarpper(list));
    }

    /**
     * 部门详情
     */
    @RequestMapping(value = "/detail/{tBrandId}")
    @ResponseBody
    public Object detail(@PathVariable("tBrandId") Integer tBrandId) {
        return itBrandService.selectById(tBrandId);
    }

    /**
     * 修改部门
     */
    @BussinessLog(value = "修改部门", key = "simplename", dict = Dict.DeptDict)
    @RequestMapping(value = "/update")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object update(TFloor tBrand) {
        if (ToolUtil.isEmpty(tBrand) || tBrand.getId() == null) {
            throw new BussinessException(BizExceptionEnum.REQUEST_NULL);
        }
        itBrandService.updateById(tBrand);
        return super.SUCCESS_TIP;
    }

    /**
     * 删除部门
     */
    @BussinessLog(value = "删除部门", key = "tBrandId", dict = Dict.DeleteDict)
    @RequestMapping(value = "/delete")
    @ResponseBody
    @Permission(Const.ADMIN_NAME)
    public Object delete(@RequestParam Long tBrandId) {

        itBrandService.deleteById(tBrandId);

        return SUCCESS_TIP;
    }
}
