package com.stylefeng.guns.modular.shop.service;

import com.stylefeng.guns.persistence.shop.model.TCart;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface ITCartService extends IService<TCart> {

    int selectOwnCartCount(Long id);

    Object selectOwnCart(Long id);
}
