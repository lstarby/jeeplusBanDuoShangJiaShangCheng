package com.stylefeng.guns.persistence.shop.dao;

import com.stylefeng.guns.common.node.ZTreeNode;
import com.stylefeng.guns.persistence.shop.model.TGoodsClass;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
  * 商品分类表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2017-05-17
 */
public interface TGoodsClassMapper extends BaseMapper<TGoodsClass> {

    List<ZTreeNode> tree();
}